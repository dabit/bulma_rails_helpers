# frozen_string_literal: true

require_relative "bulma_rails_helpers/version"
require_relative "bulma_rails_helpers/form_helper"

module BulmaRailsHelpers
  class Error < StandardError; end
  # Your code goes here...
end
