module ActionView
  module Helpers
    module FormHelper
      def bulma_text_field(name, attr, options)
        bulma_input_field(name, attr) do
          safe_join([
            text_field(name, attr, autofocus: options[:autofocus], class: "input #{bulma_input_error_class(options[:object], attr)}"),
            bulma_error_message(options[:object], attr)
          ])
        end
      end

      def bulma_email_field(name, attr, options)
        options = { autofocus: true, autocomplete: 'email' }.merge(options)
        bulma_input_field(name, attr) do
          safe_join([
            email_field(name, attr, autofocus: options[:autofocus], autocomplete: options[:autocomplete], class: "input #{bulma_input_error_class(options[:object], attr)}"),
            bulma_error_message(options[:object], attr)
          ])
        end
      end

      def bulma_password_field(name, attr, options)
        bulma_input_field(name, attr) do
          safe_join([
            password_field(name, attr, autocomplete: 'password', class: "input #{bulma_input_error_class(options[:object], attr)}"),
            bulma_error_message(options[:object], attr)
          ])
        end
      end

      def bulma_checkbox(name, attr, label, options)
        tag.div(class: :field) do
          label name, attr, class: :label do
            safe_join([check_box(name, attr, options), ' ', label])
          end
        end
      end

      def bulma_input_field(name, attr, &block)
        tag.div(class: :field) do
          label(name, attr, class: :label) +
            tag.div(class: :control) do
              block.call
            end
        end
      end

      def bulma_text_area(name, attr, options)
        options = { rows: 8 }.merge(options)
        bulma_input_field(name, attr) do
          safe_join([
            text_area(name, attr, class: "textarea #{bulma_input_error_class(options[:object], attr)}", rows: options[:rows]),
            bulma_error_message(options[:object], attr)
          ])
        end
      end

      def bulma_rich_text_area(name, attr, options)
        bulma_input_field(name, attr) do
          rich_text_area name, attr, class: "#{bulma_input_error_class(options[:object], attr)}"
        end
      end

      def bulma_collection_select(name, attr, collection, value_attr, text_attr, options, html_options)
        bulma_input_field(name, attr) do
          tag.div(class: :select) do
            collection_select name, attr, collection, value_attr, text_attr, options, html_options
          end
        end
      end

      def bulma_input_error_class(object, attr)
        object.errors.attribute_names.include?(attr) ? 'is-danger' : nil
      end

      def bulma_error_message(object, attr)
        return unless object.errors.attribute_names.include?(attr)

        tag.p(class: 'help is-danger') do
          object.errors[attr].join(', ')
        end
      end
    end
  end
end

class ActionView::Helpers::FormBuilder
  def bulma_text_field(attr, options={})
    @template.bulma_text_field(@object_name, attr, objectify_options(options))
  end

  def bulma_email_field(attr, options={})
    @template.bulma_email_field(@object_name, attr, objectify_options(options))
  end

  def bulma_password_field(attr, options={})
    @template.bulma_password_field(@object_name, attr, objectify_options(options))
  end

  def bulma_checkbox(attr, label, options={})
    @template.bulma_checkbox(@object_name, attr, label, objectify_options(options))
  end

  def bulma_text_area(attr, options={})
    @template.bulma_text_area(@object_name, attr, objectify_options(options))
  end

  def bulma_rich_text_area(attr, options={})
    @template.bulma_rich_text_area(@object_name, attr, objectify_options(options))
  end

  def bulma_collection_select(attr, collection, value_attr, text_attr, options={}, html_options={})
    @template.bulma_collection_select(@object_name, attr, collection, value_attr, text_attr, objectify_options(options), html_options)
  end
end
