# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "bulma_rails_helpers"

require "minitest/autorun"
